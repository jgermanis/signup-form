# SignupForm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` or  `npm test` to execute the unit tests via [Jest](https://jestjs.io/).

#### Running unit tests with coverage
Run `ng test --coverage` or  `npm test:coverage` to execute the unit tests with coverage reporting.


#Notes
## Email Validation

Email validation - I have not added restrictions to the email validation on form, as most of the regex validations does not completely cover RFC standarts and
also it doesn't guarantee that the email exists and is valid. I would choose email confirmation by sending confirmation to email after which user can confirm their email.
I have not implemented the full flow of email confirmation as that would be extra work,
but have added confirmed email button to success modal which would be similar as when  user  has confirmed email (login and go to home part of app).


