import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { MockModule, MockService } from 'ng-mocks';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { of, throwError } from 'rxjs';

import { SignupComponent } from './signup.component';
import { AuthService } from '../core/services/auth.service';
import { SuccessDialogComponent } from './success-dialog/success-dialog.component';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignupComponent],
      imports: [
        MockModule(MatCardModule),
        MockModule(MatFormFieldModule),
        MockModule(MatInputModule),
        MockModule(MatButtonModule),
        MockModule(MatDialogModule),
        MockModule(MatProgressSpinnerModule),
        MockModule(MatSnackBarModule),
        ReactiveFormsModule,
      ],
      providers: [
        { provide: AuthService, useValue: MockService(AuthService) },
        { provide: MatSnackBar, useValue: MockService(MatSnackBar) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    authService = TestBed.inject(AuthService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should have set up initial form', () => {
    expect(Object.keys(component.form.controls)).toEqual([
      'firstName',
      'lastName',
      'email',
      'password',
      'confirmPassword',
    ]);
    expect(component.form.valid).toBeFalsy();
  });

  describe('nameRequiredError', () => {
    it('should return true if name has required error', () => {
      expect(component.nameRequiredError).toBeTruthy();
    });
    it('should return false if name doesnt have required error', () => {
      component.form.controls.firstName.setValue('name');
      expect(component.nameRequiredError).toBeFalsy();
    });
  });

  describe('lastNameRequiredError', () => {
    it('should return true if name has required error', () => {
      expect(component.lastNameRequiredError).toBeTruthy();
    });
    it('should return false if name doesnt have required error', () => {
      component.form.controls.lastName.setValue('lastName');
      expect(component.lastNameRequiredError).toBeFalsy();
    });
  });

  describe('emailRequiredError', () => {
    it('should return true if name has required error', () => {
      expect(component.emailRequiredError).toBeTruthy();
    });
    it('should return false if name doesnt have required error', () => {
      component.form.controls.email.setValue('test@test.com');
      expect(component.emailRequiredError).toBeFalsy();
    });
  });

  describe('passwordConfirmErrorMessage', () => {
    it('should return required error message if confirm password not entered', () => {
      expect(component.passwordConfirmErrorMessage).toEqual(
        'Confirm password is required'
      );
    });
    it('should return passwords not match error message if passwords doesnt match', () => {
      component.form.controls.password.setValue('passwordA');
      component.form.controls.confirmPassword.setValue('passwordB');
      expect(component.passwordConfirmErrorMessage).toEqual(
        'Passwords does not match.'
      );
    });
    it('should return passwords not match error message if passwords doesnt match', () => {
      component.form.controls.password.setValue('passwordA');
      component.form.controls.confirmPassword.setValue('passwordA');
      expect(component.passwordConfirmErrorMessage).toBeNull();
    });
  });

  describe('passwordErrorMessage', () => {
    it('should return required error message if confirm password not entered', () => {
      expect(component.passwordErrorMessage).toEqual('Password is required.');
    });
    it('should return minlength error when length of password is less than 8', () => {
      component.form.controls.password.setValue('Abc');
      expect(component.passwordErrorMessage).toEqual(
        'Password must be 8 or more characters long.'
      );
    });
    it('should return password pattern mismatch error when not noth uppercase and lowercase characters', () => {
      component.form.controls.password.setValue('password123');
      expect(component.passwordErrorMessage).toBe(
        'Password must include lower and upper case letters.'
      );
    });
    it('should return error that can not include name in password when name included in password', () => {
      component.form.controls.password.setValue('passwordTEST');
      component.form.controls.firstName.setValue('test');
      expect(component.passwordErrorMessage).toBe(
        'Password can not include name or last name.'
      );
    });
  });

  describe('submitForm', () => {
    it('should set formClicked to true', () => {
      component.submitForm();
      expect(component.formSubmitClicked).toBeTruthy();
    });

    it('should not submit form if form is invalid', () => {
      spyOn(authService, 'signUp');
      component.submitForm();
      expect(authService.signUp).not.toHaveBeenCalled();
    });

    it('should disable form, show spinner and submit form', () => {
      spyOn(authService, 'signUp').and.returnValue(of());
      component.form.setValue({
        firstName: 'name',
        lastName: 'lastName',
        email: 'email',
        password: 'passwordA',
        confirmPassword: 'passwordA',
      });
      component.submitForm();
      expect(authService.signUp).toHaveBeenCalledWith({
        email: 'email',
        firstName: 'name',
        lastName: 'lastName',
      });
      expect(component.form.disabled).toBeTruthy();
      expect(component.signingUpInProgress).toBeTruthy();
    });

    it('should open success dialog and reset form if signup successful', fakeAsync(() => {
      spyOn(authService, 'signUp').and.returnValue(of(''));
      const dialog = TestBed.inject(MatDialog);
      spyOn(dialog, 'open');
      component.form.setValue({
        firstName: 'name',
        lastName: 'lastName',
        email: 'email',
        password: 'passwordA',
        confirmPassword: 'passwordA',
      });
      component.submitForm();
      tick();
      expect(dialog.open).toHaveBeenCalledWith(SuccessDialogComponent, {
        data: { email: 'email' },
      });
      expect(component.formSubmitClicked).toBeFalsy();
      expect(component.signingUpInProgress).toBeFalsy();
      expect(component.form.disabled).toBeFalsy();
      expect(component.form.touched).toBeFalsy();
    }));

    it('should open snackbar if signup fails', fakeAsync(() => {
      spyOn(authService, 'signUp').and.returnValue(throwError(''));
      const snackBar = TestBed.inject(MatSnackBar);
      spyOn(snackBar, 'open');
      component.form.setValue({
        firstName: 'name',
        lastName: 'lastName',
        email: 'email',
        password: 'passwordA',
        confirmPassword: 'passwordA',
      });
      component.submitForm();
      tick();
      expect(snackBar.open).toHaveBeenCalledWith(
        'There was error signing up.',
        'OK',
        { duration: 6000 }
      );
      expect(component.signingUpInProgress).toBeFalsy();
      expect(component.form.disabled).toBeFalsy();
    }));
  });
});
