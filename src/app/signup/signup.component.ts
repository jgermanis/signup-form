import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { containsNameValidator } from './validators/contains-name.validator';
import { passwordsEqualValidator } from './validators/passwords-equal.validator';
import { SuccessDialogComponent } from './success-dialog/success-dialog.component';
import { AuthService } from '../core/services/auth.service';
import { UserDetails } from '../core/models/user-details.interface';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  formSubmitClicked = false;
  signingUpInProgress = false;

  private readonly upperCaseRegex: RegExp = /[A-Z]/;
  private readonly lowerCaseRegex: RegExp = /[a-z]/;

  @ViewChild('signupForm') signupForm: ElementRef<HTMLFormElement>;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {}

  get nameRequiredError(): boolean {
    return this.form.get('firstName').hasError('required');
  }

  get lastNameRequiredError(): boolean {
    return this.form.get('lastName').hasError('required');
  }

  get emailRequiredError(): boolean {
    return this.form.get('email').hasError('required');
  }

  get passwordErrorMessage(): string | null {
    const passwordErrors = this.form.get('password').errors;
    if (!passwordErrors) {
      return null;
    }
    const errorMessageArray = Object.entries(passwordErrors);
    if (errorMessageArray.length) {
      return this.getPasswordErrorMessageFromError(errorMessageArray[0]);
    } else {
      return null;
    }
  }

  get passwordConfirmErrorMessage(): string | null {
    const confirmControl = this.form.get('confirmPassword');
    if (confirmControl.hasError('required')) {
      return 'Confirm password is required';
    }

    if (confirmControl.hasError('passwordsNotEqual')) {
      return 'Passwords does not match.';
    }
    return null;
  }

  ngOnInit(): void {
    this.form = this.fb.group(
      {
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        email: [null, Validators.required],
        password: [
          null,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(this.upperCaseRegex),
            Validators.pattern(this.lowerCaseRegex),
          ],
        ],
        confirmPassword: [null, Validators.required],
      },
      {
        validator: [containsNameValidator, passwordsEqualValidator],
      }
    );
  }

  submitForm(): void {
    this.formSubmitClicked = true;
    if (!this.form.valid) {
      return;
    } else {
      this.form.disable();
      this.signingUpInProgress = true;
      const { firstName, lastName, email } = this.form.value;
      const userDetils: UserDetails = {
        firstName,
        lastName,
        email,
      };
      this.authService.signUp(userDetils).subscribe(
        () => {
          this.signingUpInProgress = false;
          this.form.enable();
          this.formSubmitClicked = false;
          this.signupForm.nativeElement.reset();
          this.dialog.open(SuccessDialogComponent, {
            data: { email },
          });
        },
        () => {
          this.snackBar.open('There was error signing up.', 'OK', {
            duration: 6000,
          });
          this.form.enable();
          this.signingUpInProgress = false;
        }
      );
    }
  }

  private getPasswordErrorMessageFromError([key, error]: [
    string,
    any
  ]): string {
    switch (key) {
      case 'required': {
        return 'Password is required.';
      }
      case 'pattern': {
        return 'Password must include lower and upper case letters.';
      }
      case 'minlength': {
        return 'Password must be 8 or more characters long.';
      }
      default: {
        return error;
      }
    }
  }
}
