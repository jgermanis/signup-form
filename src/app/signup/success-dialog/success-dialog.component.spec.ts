import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessDialogComponent } from './success-dialog.component';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../core/services/auth.service';
import { MockService } from 'ng-mocks';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

describe('SuccessDialogComponent', () => {
  let component: SuccessDialogComponent;
  let fixture: ComponentFixture<SuccessDialogComponent>;
  let authService: AuthService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SuccessDialogComponent],
      imports: [MatDialogModule, RouterTestingModule],
      providers: [
        { provide: MatDialogRef, useValue: { close: jest.fn } },
        { provide: MAT_DIALOG_DATA, useValue: { email: 'testEmail' } },
        { provide: AuthService, useValue: MockService(AuthService) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessDialogComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('confirmedEmail', () => {
    it('should login and navigate to /', () => {
      spyOn(authService, 'logIn');
      spyOn(router, 'navigate');
      component.confirmedEmail();

      expect(authService.logIn).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
  });

  describe('DOM tests', () => {
    it('should render dialog content with injected email', () => {
      const dialogContent: HTMLElement = fixture.debugElement.query(
        By.css('mat-dialog-content')
      ).nativeElement;
      const expected =
        'Signup was successful, and we have set out confirmation email to: testEmail. Please confirm email address before starting to use the service.';

      expect(dialogContent.innerHTML.trim()).toEqual(expected);
    });
  });
});
