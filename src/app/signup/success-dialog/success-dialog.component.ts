import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success-dialog',
  templateUrl: './success-dialog.component.html',
  styleUrls: ['./success-dialog.component.scss'],
})
export class SuccessDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: { email: string },
    private authService: AuthService,
    private dialogRef: MatDialogRef<SuccessDialogComponent>,
    private router: Router
  ) {}

  ngOnInit(): void {}

  confirmedEmail(): void {
    this.authService.logIn();
    this.router.navigate(['/']);
    this.dialogRef.close();
  }
}
