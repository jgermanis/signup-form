import { FormControl, FormGroup } from '@angular/forms';
import { passwordsEqualValidator } from './passwords-equal.validator';

describe('passwordsEqualValidator', () => {
  let testForm: FormGroup;
  let passwordControl: FormControl;
  let confirmPasswordControl: FormControl;

  beforeEach(() => {
    passwordControl = new FormControl();
    confirmPasswordControl = new FormControl();

    testForm = new FormGroup({
      password: passwordControl,
      confirmPassword: confirmPasswordControl,
    });
  });

  it('should be valid for initial form', () => {
    passwordsEqualValidator(testForm);
    expect(testForm.valid).toBeTruthy();
  });

  it('should set confirmPassword to error if passwords does not match', () => {
    passwordControl.setValue('test');
    confirmPasswordControl.setValue('other');
    passwordsEqualValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(confirmPasswordControl.hasError('passwordsNotEqual')).toBeTruthy();
  });

  it('should not set confirmPassword to error if passwords match', () => {
    passwordControl.setValue('test');
    confirmPasswordControl.setValue('test');
    passwordsEqualValidator(testForm);
    expect(testForm.valid).toBeTruthy();
    expect(confirmPasswordControl.hasError('passwordsNotEqual')).toBeFalsy();
  });
});
