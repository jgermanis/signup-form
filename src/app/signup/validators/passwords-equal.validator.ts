import { AbstractControl } from '@angular/forms';

export function passwordsEqualValidator(control: AbstractControl): void {
  const password: string = control.get('password').value;
  const confirmPassword: string = control.get('confirmPassword').value;
  if (password && confirmPassword) {
    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ passwordsNotEqual: true });
    } else {
      control.get('confirmPassword').setErrors(null);
    }
  }
}
