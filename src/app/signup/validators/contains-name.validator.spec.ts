import { FormControl, FormGroup } from '@angular/forms';
import { containsNameValidator } from './contains-name.validator';

describe('containsNameValidator', () => {
  let testForm: FormGroup;
  let firstNameControl: FormControl;
  let lastNameControl: FormControl;
  let passwordControl: FormControl;

  beforeEach(() => {
    firstNameControl = new FormControl();
    lastNameControl = new FormControl();
    passwordControl = new FormControl();

    testForm = new FormGroup({
      firstName: firstNameControl,
      lastName: lastNameControl,
      password: passwordControl,
    });
  });

  it('should be valid for initial form', () => {
    containsNameValidator(testForm);
    expect(testForm.valid).toBeTruthy();
  });

  it('should set password error and invalidate form when password contains firstname', () => {
    firstNameControl.setValue('test');
    passwordControl.setValue('passwordtest');
    containsNameValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(passwordControl.hasError('noNameInPassword')).toBeTruthy();
  });

  it('should set password error and invalidate form when password contains last name', () => {
    lastNameControl.setValue('test');
    passwordControl.setValue('passwordtest');
    containsNameValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(passwordControl.hasError('noNameInPassword')).toBeTruthy();
  });

  it('should set password error and invalidate form when password contains firstname with different case', () => {
    firstNameControl.setValue('TEST');
    passwordControl.setValue('passwordtest');
    containsNameValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(passwordControl.hasError('noNameInPassword')).toBeTruthy();
  });

  it('should set add to password errors if error already exists', () => {
    firstNameControl.setValue('TEST');
    passwordControl.setValue('passwordtest');
    passwordControl.setErrors({ someError: true });
    containsNameValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(passwordControl.hasError('noNameInPassword')).toBeTruthy();
    expect(passwordControl.hasError('someError')).toBeTruthy();
    expect(Object.keys(passwordControl.errors).length).toBe(2);
  });

  it('should remove noNameInPassword from errors when password doesnt include name', () => {
    firstNameControl.setValue('test');
    passwordControl.setValue('passwordtest');
    containsNameValidator(testForm);
    expect(testForm.valid).toBeFalsy();
    expect(passwordControl.hasError('noNameInPassword')).toBeTruthy();
    firstNameControl.setValue('name');
    containsNameValidator(testForm);
    expect(testForm.valid).toBeTruthy();
    expect(passwordControl.hasError('noNameInPassword')).toBeFalsy();
  });
});
