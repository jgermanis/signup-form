import { AbstractControl } from '@angular/forms';

export function containsNameValidator(control: AbstractControl): void {
  const passwordControl: AbstractControl = control.get('password');
  const password: string = passwordControl.value;
  const name: string = control.get('firstName').value;
  const lastName: string = control.get('lastName').value;
  if (!password) {
    return;
  }
  let passwordIncludesName = false;
  if (name && password.toLowerCase().includes(name.toLowerCase())) {
    passwordIncludesName = true;
  }
  if (lastName && password.toLowerCase().includes(lastName.toLowerCase())) {
    passwordIncludesName = true;
  }

  const errors = passwordControl.errors;
  if (passwordIncludesName) {
    passwordControl.setErrors({
      ...errors,
      noNameInPassword: 'Password can not include name or last name.',
    });
  } else {
    if (errors && errors.noNameInPassword) {
      delete errors.noNameInPassword;
      passwordControl.updateValueAndValidity();
    }
  }
}
