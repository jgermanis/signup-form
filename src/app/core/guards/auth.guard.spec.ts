import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MockService } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from '../services/auth.service';

describe('AuthGuardGuard', () => {
  let guard: AuthGuard;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [{ provide: AuthService, useValue: MockService(AuthService) }],
    });
    guard = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should return true when logged in', () => {
    spyOn(authService, 'isAuthenticated').and.returnValue(true);
    expect(
      guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)
    ).toBeTruthy();
  });

  it('should return urlTree when not logged in', () => {
    spyOn(authService, 'isAuthenticated').and.returnValue(false);
    expect(
      guard
        .canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot)
        .toString()
    ).toBe('/signup');
  });
});
