import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { AuthService } from './auth.service';
import { UserDetails } from '../models/user-details.interface';
import { environment } from '../../../environments/environment';

describe('AuthService', () => {
  let service: AuthService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(AuthService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('logIn', () => {
    it('should set authenticated to true', () => {
      expect(service.isAuthenticated()).toBeFalsy();
      service.logIn();
      expect(service.isAuthenticated()).toBeTruthy();
    });
  });

  describe('singIn', () => {
    it('should send post request with provided userDetails', () => {
      const testUserDetails: UserDetails = {
        firstName: 'name',
        lastName: 'lastName',
        email: 'email',
      };
      const mockResponse = { test: 'Test' };

      service
        .signUp(testUserDetails)
        .subscribe((data) => expect(data).toEqual(mockResponse));

      const req = httpTestingController.expectOne(
        environment.apiUrl + '/users'
      );

      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(testUserDetails);

      req.flush(mockResponse);

      httpTestingController.verify();
    });
  });
});
