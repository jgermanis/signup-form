import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserDetails } from '../models/user-details.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  private isLoggedIn = false;

  isAuthenticated(): boolean {
    return this.isLoggedIn;
  }

  logIn(): void {
    this.isLoggedIn = true;
  }
  signUp(userDetails: UserDetails): Observable<any> {
    return this.http.post(environment.apiUrl + '/users', userDetails);
  }
}
